{
  "id": "001",
  "name": "Пижама для девочек",
  "vendor": "02765/46",
  "rating":"4.2787",
  "reviews": [
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    },
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    },
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    },
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    },
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    },
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    },
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    },
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    },
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    },
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    },
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    },
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    },
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    },
    {
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, enim doloribus repellat qui corrupti quaerat quo nobis rem quasi at, architecto commodi. Fugiat dolores sunt accusamus modi sed illo? Vitae."
    }
  ],
  "price": "1500",
  "price_discounted": "800",
  "discounts": [
    {
      "id":"discounts-01",
      "type":"discount",
      "value":"0.36"
    },
    {
      "id":"discounts-02",
      "type":"promo",
      "value":"0.8"
    }
  ],
  "sizes": [
    {
      "id":"01",
      "name":"XS"
    },
    {
      "id":"02",
      "name":"S",
      "out_of_stock":"true"
    },
    {
      "id":"03",
      "name":"M"
    },
    {
      "id":"04",
      "name":"L"
    },
    {
      "id":"05",
      "name":"XL"
    }
  ],
  "images": [
    {
      "id": "011",
      "preview": "https://i.ibb.co/GsMjHkf/main-1.jpg",
      "full": "https://i.ibb.co/4sSx1Mw/main-1.jpg"
    },
    {
      "id": "012",
      "preview": "https://i.ibb.co/yBpsLKG/main-2.jpg",
      "full": "https://i.ibb.co/PzMCBJ3/main-2.jpg"
    },
    {
      "id": "013",
      "preview": "https://i.ibb.co/t3zmLNN/main-3.jpg",
      "full": "https://i.ibb.co/x3YCFcc/main-3.jpg"
    },
    {
      "id": "014",
      "preview": "https://i.ibb.co/5jmQ9dH/main-4.jpg",
      "full": "https://i.ibb.co/HP6c208/main-4.jpg"
    },
    {
      "id": "015",
      "preview": "https://i.ibb.co/hF9hV09/main-5.jpg",
      "full": "https://i.ibb.co/jkfBDtf/main-5.jpg"
    }
  ],
  "gallery": [
    {
      "id":"021",
      "likes": "100",
      "href":"#",
      "image":"https://i.ibb.co/PzMCBJ3/main-2.jpg"
    },
    {
      "id":"022",
      "likes": "95",
      "href":"#",
      "image":"https://i.ibb.co/x3YCFcc/main-3.jpg"
    },
    {
      "id":"023",
      "likes": "54",
      "href":"#",
      "image":"https://i.ibb.co/HP6c208/main-4.jpg"
    },
    {
      "id":"024",
      "likes": "31",
      "href":"#",
      "image":"https://i.ibb.co/jkfBDtf/main-5.jpg"
    },
    {
      "id":"025",
      "likes": "0",
      "href":"#",
      "image":"https://i.ibb.co/tmpbwZP/main-6.jpg"
    }
  ]
}