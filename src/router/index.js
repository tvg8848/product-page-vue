import Vue from 'vue'
import VueRouter from 'vue-router'
import ProductCard from '../views/ProductCard.vue'
import UiKit from '../components/UiKit.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: ProductCard
  },
  {
    path: '/product-card',
    name: 'ProductCard',
    component: ProductCard
  },
  {
    path: '/ui',
    name: 'ui-kit',
    component: UiKit
  },
  {
    path: '/404',
    name: '404',
    component: require('@src/views/_404.vue').default,
    // Allows props to be passed to the 404 page through route
    // params, such as `resource` to define what wasn't found.
    props: true,
  },
  // Redirect any unmatched routes to the 404 page. This may
  // require some server configuration to work in production:
  // https://router.vuejs.org/en/essentials/history-mode.html#example-server-configurations
  {
    path: '*',
    redirect: '404',
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
