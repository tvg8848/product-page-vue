function pushToLocalStorage(name, data) {
  if (typeof data === 'object') {
    data = JSON.stringify(data)
  }

  window.localStorage.setItem(name, data)
}

export default pushToLocalStorage
