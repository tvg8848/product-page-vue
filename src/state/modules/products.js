import axios from 'axios'

export const actions = {
  getProductData({ commit, dispatch, getters }, id = null) {
    // fake API
    return axios.get('https://mocki.io/v1/' + id)
  },
}
