import axios from 'axios'

export const state = {
  subscribeLoading: false,
}

export const mutations = {
  TOGGLE_LOADER(state, payload) {
    state.subscribeLoading = payload
    console.log('state', state.subscribeLoading)
  }
}

export const actions = {
  async mailSubscribe({ commit }, data = {}) {
    commit('TOGGLE_LOADER', true)
    try {
      await axios.post('https://dummyjson.com/products/add', data)
    } finally {
      commit('TOGGLE_LOADER', false)
    }
  },
}

export const getters = {
  subscribeLoading(state) {
    return state.subscribeLoading
  }
}
