import pushToLocalStorage from '@src/utils/local-storage-handler'

export const state = {
  cart: [],
  fav: [],
}

export const mutations = {
  ADD_TO_CART(state, payload) {
    state.cart.push(payload)
  },
  ADD_TO_FAV(state, payload) {
    state.fav.push(payload)
  },
  UPDATE_CART(state, payload) {
    state.cart = payload.list
  },
  UPDATE_FAV(state, payload) {
    state.fav = payload.list
  },
  CLEAR_CART(state) {
    state.cart = []
  },
  CLEAR_FAV(state) {
    state.fav = []
  }
}

export const actions = {
  ADD_ITEM({ commit }, payload) {
    if (payload.type === 'cart') {
      commit('ADD_TO_CART', payload)
      pushToLocalStorage('product_page_cart', state.cart)
    } else if (payload.type === 'fav') {
      commit('ADD_TO_FAV', payload)
      pushToLocalStorage('product_page_fav', state.fav)
    }
  },
  UPDATE_CART({ commit }, payload) {
    commit('UPDATE_CART', payload)
  },
  UPDATE_FAV({ commit }, payload) {
    commit('UPDATE_FAV', payload)
  },
  CLEAR({ commit }, payload) {
    if (payload.type === 'cart') {
      commit('CLEAR_CART')
      // window.localStorage.setItem('product_page_cart', JSON.stringify([]))
      pushToLocalStorage('product_page_cart', [])
    } else if (payload.type === 'fav') {
      commit('CLEAR_FAV')
      pushToLocalStorage('product_page_fav', [])
      // window.localStorage.setItem('product_page_fav', JSON.stringify([]))
    }
  }
}

export const getters = {
  cartCount(state) {
    if (state.cart && state.cart.length) {
      return state.cart.length
    } else {
      return null
    }
  },
  cartList(state) {
    return state.cart
  },
  favCount(state) {
    if (state.fav && state.fav.length) {
      return state.fav.length
    } else {
      return null
    }
  },
}
