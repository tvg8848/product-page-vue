export const state = {
  message: '',
  type: ''
}

export const mutations = {
  SET_MESSAGE(state, payload) {
    state.message = payload.message
  },
  SET_TYPE(state, payload) {
    state.type = payload.type
  }
}

export const actions = {
  SET_MESSAGE({ commit }, payload) {
    commit('SET_MESSAGE', payload)
    commit('SET_TYPE', payload)
  }
}

export const getters = {
  message(state) {
    return state.message
  },
  type(state) {
    return state.type
  }
}
