import { mapActions } from 'vuex'

export const productMethods = mapActions('products', [
  'getProductData'
])

export const userMethods = mapActions('users', [
  'mailSubscribe',
])
