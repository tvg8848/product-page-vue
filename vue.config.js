/** @type import('@vue/cli-service').ProjectOptions */
module.exports = {
  indexPath: './index.html',
  //  configureWebpack: {
  //    plugins: [new BundleAnalyzerPlugin()]
  //  },
  // https://github.com/neutrinojs/webpack-chain/tree/v4#getting-started
  
  publicPath: process.env.NODE_ENV === 'production'? '/product-page-vue/' : '/',

  chainWebpack (config) {
    // Set up all the aliases we use in our app.
    config.resolve.alias.clear().merge(require('./aliases.config').webpack)
  },
  css: {
    // Enable CSS source maps.
    sourceMap: true
  },
  devServer: {
    disableHostCheck: true
  }
}
